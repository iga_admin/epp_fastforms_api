﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using FastFormsEppApi.Data;

namespace FastForms.Api.Lib
{
    public class ApiIdentity : IIdentity
    {
        public ff_login User { get; private set; }

        public ApiIdentity(ff_login user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            this.User = user;
        }

        public string Name
        {
            get { return this.User.ff_user.fname + " " + this.User.ff_user.lname; }
        }

        public string Email
        {
            get { return this.User.email; }
        }

        public decimal UserID
        {
            get { return this.User.id; }
        }

        public string AuthenticationType
        {
            get { return "Basic"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }
    }
}