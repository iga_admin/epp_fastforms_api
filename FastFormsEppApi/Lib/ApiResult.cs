﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FastForms.Api.Lib
{
    public class ApiResult
    {
        public class api_error
        {
            public string error { get; set; }
            public string heading { get; set; }
            public string debug { get; set; }
        }

        public api_error error { get; set; }

        public bool success { get; set; }

        public dynamic data { get; set; }
    }
}