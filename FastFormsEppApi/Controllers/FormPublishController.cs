﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FastForms.Api.Lib;
using FastFormsEppApi.Data;
using System.Web;


namespace FastForms.Api.Controllers
{
    [RoutePrefix("api/formpublish")]
    public class FormPublishController : ApiController
    {
        //[Route("SaveUser")]
        //[HttpPost]
        //public ApiResult SaveUser(string fname, string lname, string email, string company_name)
        //{
        //    ApiResult result = new Lib.ApiResult();
        //    FastFormsDBEntities db = new FastFormsDBEntities();
        //    try
        //    {

        //        if (!string.IsNullOrEmpty(fname) && !string.IsNullOrEmpty(lname) && !string.IsNullOrEmpty(email))
        //        {
        //            ff_user check_user = db.ff_user.Where(u => u.email == email && u.deleted_date == null).FirstOrDefault();

        //            if (check_user == null)
        //            {
        //                ff_user form_user = new ff_user();
        //                form_user.fname = fname;
        //                form_user.lname = lname;
        //                form_user.email = email;
        //                form_user.company_name = company_name;
        //                form_user.created_date = DateTime.Now;
        //                db.ff_user.Add(form_user);
        //                db.SaveChanges();

        //                ff_login form_user_login = new ff_login();
        //                form_user_login.user_id = form_user.id;
        //                form_user_login.email = form_user.email;
        //                form_user_login.password = CreateRandomPassword();
        //                form_user_login.token = Guid.NewGuid().ToString();
        //                form_user_login.created_date = DateTime.Now;
        //                db.ff_login.Add(form_user_login);
        //                db.SaveChanges();
        //                result.success = true;
        //                result.data = form_user;
        //            }
        //            else
        //            {
        //                result.success = true;
        //                result.data = check_user;
        //            }

        //        }
        //        else
        //        {
        //            result.success = false;
        //            result.error = new ApiResult.api_error();
        //            result.error.heading = "SignUp";
        //            result.error.error = "Values can not be null or empty.";
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //    }
        //    return result;
        //}
        [Route("GenerateFormRequest")]
        [HttpPost]
        // [FastFormsAuthorization]
        public ApiResult GenerateFormRequest(string fName, string lName, string email, string companyName, decimal FormID)
        {

            ApiResult result = new Lib.ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            try
            {

                var UserToken = "";
                decimal UserId = 0;
                if (string.IsNullOrEmpty(fName) || string.IsNullOrEmpty(lName) || string.IsNullOrEmpty(email))
                {
                    ff_user check_user = db.ff_user.Where(u => u.email == email && u.deleted_date == null).FirstOrDefault();

                    if (check_user == null)
                    {
                        ff_user form_user = new ff_user();
                        form_user.fname = fName;
                        form_user.lname = lName;
                        form_user.email = email;
                        form_user.company_name = companyName;
                        form_user.created_date = DateTime.Now;
                        db.ff_user.Add(form_user);
                        db.SaveChanges();

                        ff_login form_user_login = new ff_login();
                        form_user_login.user_id = form_user.id;
                        form_user_login.email = form_user.email;
                        form_user_login.password = CreateRandomPassword();
                        form_user_login.token = Guid.NewGuid().ToString();
                        form_user_login.created_date = DateTime.Now;
                        db.ff_login.Add(form_user_login);
                        db.SaveChanges();
                        result.success = true;
                        result.data = form_user;

                        UserToken = form_user_login.token;
                        UserId = form_user.id;
                    }
                    else
                    {
                        result.success = true;
                        result.data = check_user;
                        UserToken = check_user.ff_login.Select(t => t.token).FirstOrDefault();
                        UserId = check_user.id;
                    }
                    // Check Request Already Exist with same FormId and same UserId
                    var checkRequestExist = db.ff_form_request.Where(r => r.user_id == UserId && r.form_id == FormID && r.deleted_date == null).FirstOrDefault();

                    if (checkRequestExist == null)
                    {
                        ff_form_request form_request = new ff_form_request();
                        form_request.form_id = FormID;
                        form_request.user_id = UserId;
                        form_request.request_token = Guid.NewGuid().ToString();
                        form_request.created_date = DateTime.Now;
                        db.ff_form_request.Add(form_request);
                        db.SaveChanges();

                        var RequestToken = form_request.request_token;
                        string FormRequestURl = "?FormId=" + encoding(FormID.ToString()) + "&UserToken=" + UserToken + "&RequestToken=" + RequestToken + "&FormType=" + checkRequestExist.ff_forms.ff_form_type.form_type;

                        result.success = true;
                        result.data = FormRequestURl;
                    }
                    else
                    {
                        result.success = true;
                        string FormRequestURl = "?FormId=" + encoding(checkRequestExist.form_id.ToString()) + "&UserToken=" + UserToken + "&RequestToken=" + checkRequestExist.request_token + "&FormType=" + checkRequestExist.ff_forms.ff_form_type.form_type;
                        result.data = FormRequestURl;
                    }


                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "Generate Form Request";
                    result.error.error = "Values can not be null or empty.";
                }

                // Check if user is already exist with email or not
                // If Exist then do nothing.
                // If not exists then insert User

                // Now we have UserID

                // Fetch UserToken from FormID



                // Store UserID , RequestToken , FormID, etc... in FormRequestTable
            }
            catch (Exception ex)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Error";
                result.error.error = ex.Message.ToString();
            }

            return result;
        }


        // New Updated GenerateFormRequest Method 
        [Route("GenerateRequest")]
        [HttpGet, HttpPost]
        [FastFormsAuthorization]
        //[EnableCors("*","*","*")]
        public ApiResult GenerateRequest(decimal id)
        {
            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

            ApiResult result = new Lib.ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            try
            {
                var checkRequestExist = db.ff_form_request.Where(r => r.user_id == user.user_id && r.form_id == id && r.deleted_date == null).FirstOrDefault();
                if (checkRequestExist == null)
                {
                    ff_form_request form_request = new ff_form_request();
                    form_request.form_id = id;
                    form_request.user_id = user.user_id;
                    form_request.request_token = Guid.NewGuid().ToString();
                    form_request.created_date = DateTime.Now;
                    db.ff_form_request.Add(form_request);
                    db.SaveChanges();

                    var RequestToken = form_request.request_token;
                    //var FormType = form_request.ff_forms.ff_form_type.form_type;
                    var FormType = db.ff_forms.Where(f => f.id == id).Select(t => t.ff_form_type.form_type).FirstOrDefault();
                    string FormRequestURl = "?FormId=" + encoding(id.ToString()) + "&UserToken=" + user.token + "&RequestToken=" + RequestToken + "&FormType=" + FormType;

                    result.success = true;
                    result.data = FormRequestURl;
                }
                else
                {
                    result.success = true;
                    string FormRequestURl = "?FormId=" + encoding(checkRequestExist.form_id.ToString()) + "&UserToken=" + user.token + "&RequestToken=" + checkRequestExist.request_token + "&FormType=" + checkRequestExist.ff_forms.ff_form_type.form_type;
                    result.data = FormRequestURl;
                }

            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Error";
                result.error.error = e.Message.ToString();
            }

            return result;
        }

        private static string CreateRandomPassword(int length = 8)
        {
            // Create a string of characters, numbers, special characters that allowed in the password  
            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            Random random = new Random();

            // Select one random character at a time from the string  
            // and create an array of chars  
            char[] chars = new char[length];
            for (int i = 0; i < length; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }

        public string encoding(string toEncode)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding(28591).GetBytes(toEncode);
            string toReturn = System.Convert.ToBase64String(bytes);
            return toReturn;
        }

    }
}
