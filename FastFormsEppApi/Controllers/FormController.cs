﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FastForms.Api.Lib;
using System.Dynamic;
using FastForms.Api.Models;
using System.Web;
using FastFormsEppApi.Data;

namespace FastForms.Api.Controllers
{
    [RoutePrefix("api/form")]
    public class FormController : ApiController
    {
        //[Route("GetForms")]
        //[HttpGet]
        //[FastFormsAuthorization]
        //public ApiResult GetForms(string project_token)
        //{
        //    FastFormsDBEntities db = new FastFormsDBEntities();
        //    ApiResult result = new ApiResult();

        //    var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

        //    try
        //    {
        //        if (!String.IsNullOrEmpty(project_token))
        //        {
        //            var tok = 0;
        //            var project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null).Select(p => p.id).FirstOrDefault();

        //            var form_data = db.ff_forms.Where(f => f.project_id == project_id && f.deleted_date == null && f.created_by == user.user_id).ToList();
        //            if (form_data.Count != 0)
        //            {
        //                tok = Convert.ToInt32(form_data[0].created_by);


        //                var f_data = new List<ExpandoObject>();
        //                var user_token = db.ff_login.Where(t => t.user_id == tok && t.deleted_date == null).Select(p => p.token).FirstOrDefault();
        //                foreach (var item in form_data)
        //                {
        //                    dynamic expando = new ExpandoObject();

        //                    expando.id = item.id;
        //                    expando.attr_id = item.form_attr_id;
        //                    expando.name = item.name;
        //                    expando.created_by = item.ff_project.ff_user.fname + " " + item.ff_project.ff_user.lname;
        //                    expando.created_date = item.created_date;
        //                    expando.is_active = item.is_active;
        //                    expando.comment = item.description;
        //                    expando.forms_type_id = item.forms_type_id;
        //                    expando.form_link = "?form_id=" + item.id.ToString() + "&user_token=" + user_token;

        //                    f_data.Add(expando);
        //                }

        //                result.data = f_data;
        //                result.success = true;
        //            }
        //            else
        //            {
        //                result.success = false;
        //                result.error = new ApiResult.api_error();
        //                result.error.heading = "Error";
        //                result.error.error = "This ProjectToken doesn't belong to UserToken";
        //            }
        //        }

        //        else
        //        {
        //            result.success = false;
        //            result.error = new ApiResult.api_error();

        //            result.error.heading = "Error";
        //            result.error.error = "Token Not Found.";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return result;
        //}



        [Route("GetForms")]
        [HttpGet]
        [FastFormsAuthorization]
        public ApiResult GetForms(string project_token)
        {
            FastFormsDBEntities db = new FastFormsDBEntities();
            ApiResult result = new ApiResult();

            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

            try
            {
                if (!String.IsNullOrEmpty(project_token))
                {
                    var tok = 0;
                    var project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null).Select(p => p.id).FirstOrDefault();

                    var form_data = db.ff_forms.Where(f => f.project_id == project_id && f.deleted_date == null && f.created_by == user.user_id).ToList();
                    if (form_data.Count != 0)
                    {
                        tok = Convert.ToInt32(form_data[0].created_by);


                        var f_data = new List<ExpandoObject>();
                        var user_token = db.ff_login.Where(t => t.user_id == tok && t.deleted_date == null).Select(p => p.token).FirstOrDefault();
                        foreach (var item in form_data)
                        {
                            dynamic expando = new ExpandoObject();

                            expando.id = item.id;
                            expando.attr_id = item.form_attr_id;
                            expando.name = item.name;
                            expando.created_by = item.ff_project.ff_user.fname + " " + item.ff_project.ff_user.lname;
                            expando.created_date = item.created_date;
                            expando.is_active = item.is_active;
                            expando.comment = item.description;
                            expando.forms_type_id = item.forms_type_id;
                            expando.form_link = "?form_id=" + item.id.ToString() + "&user_token=" + user_token;

                            f_data.Add(expando);
                        }

                        result.data = f_data;
                        result.success = true;
                    }
                    else
                    {
                        var proj_user = db.ff_project_users.Where(f => f.project_id == project_id && f.user_id == user.user_id).ToList();
                        if (proj_user.Count != 0)
                        {
                            form_data = db.ff_forms.Where(f => f.project_id == project_id && f.deleted_date == null).ToList();

                            tok = Convert.ToInt32(form_data[0].created_by);

                            var f_data = new List<ExpandoObject>();
                            var user_token = db.ff_login.Where(t => t.user_id == tok && t.deleted_date == null).Select(p => p.token).FirstOrDefault();
                            foreach (var item in form_data)
                            {
                                dynamic expando = new ExpandoObject();

                                expando.id = item.id;
                                expando.attr_id = item.form_attr_id;
                                expando.name = item.name;
                                expando.created_by = item.ff_project.ff_user.fname + " " + item.ff_project.ff_user.lname;
                                expando.created_date = item.created_date;
                                expando.is_active = item.is_active;
                                expando.comment = item.description;
                                expando.forms_type_id = item.forms_type_id;
                                expando.form_link = "?form_id=" + item.id.ToString() + "&user_token=" + user_token;

                                f_data.Add(expando);
                            }

                            result.data = f_data;
                            result.success = true;
                        }

                        else
                        {
                            result.success = false;
                            result.error = new ApiResult.api_error();
                            result.error.heading = "Error";
                            result.error.error = "This ProjectToken doesn't belong to UserToken";
                        }
                    }
                }

                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();

                    result.error.heading = "Error";
                    result.error.error = "Token Not Found.";
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }



        [Route("AddForm")]
        [HttpPost]
        [FastFormsAuthorization]
        public ApiResult AddForm(string project_token, string form_name, string forms_type_id, string form_attr_id, string comments, string active)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

            try
            {
                var project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null
                    && (t.user_id == user.user_id)).Select(p => p.id).FirstOrDefault();

                if (project_id == 0)
                {
                    decimal? parent_Id = db.ff_user.Where(y => y.id == user.user_id).Select(x => x.parent_user_id).FirstOrDefault();

                    project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null
                     && (t.user_id == parent_Id)).Select(p => p.id).FirstOrDefault();

                    user = db.ff_login.Where(x => x.id == parent_Id).FirstOrDefault();
                }

                var name_check = db.ff_forms.Where(c => c.name == form_name && c.project_id == project_id && c.created_by == user.user_id && c.deleted_date == null).Count();

                if (name_check != 0)
                {

                    result.data = 0;
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "FormName Already Exist";

                }
                else
                {

                    //var project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null
                    //&& (t.user_id == user.user_id)).Select(p => p.id).FirstOrDefault();

                    //if (project_id == 0)
                    //{
                    //    decimal? parent_Id = db.ff_user.Where(y => y.id == user.user_id).Select(x => x.parent_user_id).FirstOrDefault();

                    //    project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null
                    //     && (t.user_id == parent_Id)).Select(p => p.id).FirstOrDefault();

                    //    user = db.ff_login.Where(x => x.id == parent_Id).FirstOrDefault();
                    //}


                    if (!string.IsNullOrEmpty(form_name) && !string.IsNullOrEmpty(forms_type_id) && !string.IsNullOrEmpty(form_attr_id) && project_id != 0)
                    {
                        ff_forms form = new ff_forms();

                        form.project_id = project_id;
                        form.name = form_name;
                        form.form_attr_id = form_attr_id;
                        form.is_active = active;//"ACTIVE";
                        form.description = comments;
                        form.created_date = DateTime.Now;
                        form.forms_type_id = Decimal.Parse(forms_type_id);
                        form.created_by = user.user_id;

                        db.ff_forms.Add(form);
                        db.SaveChanges();

                        ff_form_version version = new ff_form_version();
                        version.form_id = form.id;
                        version.created_date = DateTime.Now;

                        db.ff_form_version.Add(version);
                        db.SaveChanges();

                        result.data = form.id;
                        result.success = true;

                        return result;
                    }
                    else
                    {
                        result.success = false;
                        result.error = new ApiResult.api_error();
                        result.error.heading = "Create Form";



                        if (project_id == 0)
                        {
                            result.error = new ApiResult.api_error();
                            result.error.error = "Error in Project Token";
                        }
                        else
                        {
                            result.error = new ApiResult.api_error();
                            result.error.error = "Values Cannot be NULL Or Empty.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Create Form";
                result.error.error = e.Message.ToString();
            }
            return result;
        }


        [Route("CheckFormExist")]
        [HttpGet]
        [FastFormsAuthorization]
        public ApiResult CheckFormExist(string project_token, decimal form_id)
        {

            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

            try
            {
                var project_id = db.ff_project.Where(t => t.auth_token == project_token && t.deleted_date == null).Select(p => p.id).FirstOrDefault();

                var form = db.ff_forms.Where(f => f.id == form_id && f.deleted_date == null).Count();

                if (form > 0)
                {
                    result.success = false;
                    //result.error.heading = "Form Exist or Not.";
                    //result.error.error = "Form is already exist.";
                }
                else
                {
                    result.success = true;
                }

            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Check Form Exist";
                result.error.error = e.Message.ToString();
            }
            return result;
        }

        [Route("DeleteForm")]
        [HttpPost]
        [FastFormsAuthorization]
        public ApiResult DeleteForm(decimal form_id = 0)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;

            ff_user parent = db.ff_user.Where(x => x.id == user.user_id).FirstOrDefault();

            try
            {
                ff_forms form = db.ff_forms.Where(f => f.id == form_id && f.created_by == user.user_id && f.deleted_date == null).FirstOrDefault();
                if (form == null)
                {
                    form = db.ff_forms.Where(f => f.id == form_id && f.created_by == parent.parent_user_id && f.deleted_date == null).FirstOrDefault();
                }
                if (form != null)
                {
                    form.deleted_date = DateTime.Now;
                    db.SaveChanges();

                    result.success = true;
                    result.data = "Form Successfully Deleted.";
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "Delete Form";
                    result.error.error = "Form Id not found.";
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Delete Form";
                result.error.error = e.Message.ToString();
            }
            return result;
        }
        [Route("EditForm")]
        [HttpPost]
        [FastFormsAuthorization]
        public ApiResult EditForm(Form update)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();

            var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;
            ff_user parent = db.ff_user.Where(x => x.id == user.user_id).FirstOrDefault();
            decimal? ID = user.user_id;
            try
            {
                ff_forms form = new ff_forms();
                form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null);
               
                if (form != null)
                {   
                        var name_check = db.ff_forms.Where(c => c.id != update.id && c.name == update.name && c.project_id == form.project_id && c.deleted_date == null).Count();
                        if (name_check != 0)
                        {
                            result.data = name_check;
                            result.success = false;
                            result.error = new ApiResult.api_error();
                            result.error.heading = "FormName Already Exist";

                        }
                        else
                        {
                            form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null);
                            if (!string.IsNullOrEmpty(update.name) && !string.IsNullOrEmpty(update.forms_type_id))
                            {
                                form.name = update.name;
                                form.forms_type_id = Decimal.Parse(update.forms_type_id);
                                form.description = update.description;
                                form.is_active = update.is_active;

                                db.SaveChanges();
                                update.id = (int)form.id;
                                update.name = form.name;
                                update.forms_type_id = form.forms_type_id.ToString();

                                update.description = form.description;
                                update.is_active = form.is_active;

                                result.data = update;
                                result.success = true;
                                return result;
                            }
                            else
                            {
                                result.success = false;
                                result.error = new ApiResult.api_error();
                                result.error.heading = "Error";
                                result.error.error = "Values Cannot be NULL Or Empty.";
                            }


                        }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "Edit Form";
                    result.error.error = "Form Not Found.";

                }

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //public ApiResult EditForm(Form update)
        //{
        //    ApiResult result = new ApiResult();
        //    FastFormsDBEntities db = new FastFormsDBEntities();

        //    var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;
        //    try
        //    {
        //        var name_exists_by_id = db.ff_forms.Where(f => f.id == update.id && f.name == update.name && f.created_by == user.user_id).Any();
        //        if (name_exists_by_id)
        //        {
        //            result.data = update.editFormById(update);
        //            result.success = true;

        //        }
        //        else
        //        {
        //            var name_exists = db.ff_forms.Where(c => c.name == update.name && c.deleted_date == null && c.created_by == user.user_id).Count();

        //            if (name_exists != 0)
        //            {

        //                result.data = name_exists;
        //                result.success = false;
        //                result.error = new ApiResult.api_error();

        //                result.error.heading = "FormName Already Exist";

        //            }
        //            else
        //            {
        //                result.data = update.editFormById(update);
        //                result.success = true;

        //            }

        //        }
        //        return result;
        //    }

        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}



        //public ApiResult EditForm(Form update)
        //{
        //    ApiResult result = new ApiResult();
        //    FastFormsDBEntities db = new FastFormsDBEntities();

        //    var user = ((ApiIdentity)HttpContext.Current.User.Identity).User;
        //    ff_user parent = db.ff_user.Where(x => x.id == user.user_id).FirstOrDefault();
        //    decimal? ID = user.user_id;
        //    try
        //    {
        //        ff_forms form = new ff_forms();
        //        form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null && c.created_by == user.user_id);

        //        if(form== null)
        //        {
        //            form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null && c.created_by == parent.parent_user_id);
        //            ID = parent.parent_user_id;
        //        }
        //        if (form != null)
        //        {
        //            var name_exists_by_id = db.ff_forms.Where(f => f.id == update.id && f.project_id == form.project_id && f.name == update.name && f.created_by == ID).Any();
        //            if (name_exists_by_id)
        //            {

        //                form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null && c.created_by == ID);

        //                if (!string.IsNullOrEmpty(update.name) && !string.IsNullOrEmpty(update.forms_type_id))
        //                {
        //                    form.name = update.name;
        //                    form.forms_type_id = Decimal.Parse(update.forms_type_id);
        //                    form.description = update.description;
        //                    form.is_active = update.is_active;

        //                    //  form.message = update.message;
        //                    db.SaveChanges();
        //                    update.id = (int)form.id;
        //                    update.name = form.name;
        //                    update.forms_type_id = form.forms_type_id.ToString();

        //                    update.description = form.description;
        //                    update.is_active = form.is_active;
        //                    result.data = update;
        //                    result.success = true;
        //                    return result;
        //                }
        //                else
        //                {
        //                    result.success = false;
        //                    result.error = new ApiResult.api_error();
        //                    result.error.heading = "Error";
        //                    result.error.error = "Values Cannot be NULL Or Empty.";
        //                }

        //            }

        //            else
        //            {
        //                var name_check = db.ff_forms.Where(c => c.name == update.name && c.project_id == form.project_id && c.created_by == ID && c.deleted_date == null).Count();
        //                if (name_check != 0)
        //                {
        //                    result.data = name_check;
        //                    result.success = false;
        //                    result.error = new ApiResult.api_error();
        //                    result.error.heading = "FormName Already Exist";

        //                }
        //                else
        //                {
        //                    form = db.ff_forms.FirstOrDefault(c => c.id == update.id && c.deleted_date == null && c.created_by == ID);
        //                    if (!string.IsNullOrEmpty(update.name) && !string.IsNullOrEmpty(update.forms_type_id))
        //                    {
        //                        form.name = update.name;
        //                        form.forms_type_id = Decimal.Parse(update.forms_type_id);
        //                        form.description = update.description;
        //                        form.is_active = update.is_active;

        //                        db.SaveChanges();
        //                        update.id = (int)form.id;
        //                        update.name = form.name;
        //                        update.forms_type_id = form.forms_type_id.ToString();

        //                        update.description = form.description;
        //                        update.is_active = form.is_active;

        //                        result.data = update;
        //                        result.success = true;
        //                        return result;
        //                    }
        //                    else
        //                    {
        //                        result.success = false;
        //                        result.error = new ApiResult.api_error();
        //                        result.error.heading = "Error";
        //                        result.error.error = "Values Cannot be NULL Or Empty.";
        //                    }


        //                }

        //            }

        //        }
        //        else
        //        {
        //            result.success = false;
        //            result.error = new ApiResult.api_error();
        //            result.error.heading = "Edit Form";
        //            result.error.error = "Form Not Found.";

        //        }

        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
    }
}

public class update
{
    public decimal form_id { get; set; }
    public string description { get; set; }
    public string is_active { get; set; }
    public string forms_type_id { get; set; }
    public string name { get; set; }
}
