﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Security.Principal;

using System.Web.Http.Controllers;
using System.Web;
using FastForms.Api.Lib;
using FastFormsEppApi.Data;

namespace FastForms.Api.Controllers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class FastFormsAuthorization : AuthorizeAttribute
    {
        FastFormsDBEntities db = new FastFormsDBEntities();

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            IEnumerable<string> values;
            if (filterContext.Request.Headers.TryGetValues("ff-key",out values))
            {
                string authenticationToken = values.First();
                if (ApiValidatorService.IsValid(authenticationToken))
                {
                    HttpContext.Current.Response.AddHeader("authenticationToken", authenticationToken);
                    HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                }
                else
                {
                    HttpContext.Current.Response.AddHeader("authenticationToken", authenticationToken);
                    HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                    filterContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    return;
                }
            }
            else
            {
                HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                filterContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                return;
            }
             //   base.OnAuthorization(actionContext);
        }

        public static class ApiValidatorService
        {

            public static bool IsValid(string key)
            {
                FastFormsDBEntities db = new FastFormsDBEntities();

                if (key != null || key != "")
                {
                    //   string encodeKey = EncryptionModel.Encode(key.ToString());
                    ff_login user = db.ff_login.Where(m => m.token == key).FirstOrDefault();
                    if (user != null)
                    {
                        HttpContext.Current.User = new GenericPrincipal(new ApiIdentity(user), new string[] { });
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
