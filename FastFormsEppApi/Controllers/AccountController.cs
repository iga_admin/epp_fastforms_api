﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FastForms.Api.Lib;
using FastFormsEppApi.Data;
using FastForms.Api.Models;


namespace FastForms.Api.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        [Route("GetName")]
        [HttpGet]
        public string GetName()
        {
            return "Nitesh";
        }
        [Route("SignUp")]
        [HttpPost]
        public ApiResult SignUp(Registration objUser)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                if (!string.IsNullOrEmpty(objUser.fname) && !string.IsNullOrEmpty(objUser.lname) && !string.IsNullOrEmpty(objUser.email) && !string.IsNullOrEmpty(objUser.contact) && !string.IsNullOrEmpty(objUser.password))
                {
                    var email_check = db.ff_login.Where(e => e.email == objUser.email && e.deleted_date == null).FirstOrDefault();
                    if (email_check == null)
                    {
                        ff_user objRegister = new ff_user();
                        objRegister.fname = objUser.fname;
                        objRegister.lname = objUser.lname;
                        objRegister.email = objUser.email;
                        objRegister.contact = objUser.contact;
                        objRegister.user_image = objUser.user_image;
                        objRegister.created_date = DateTime.Now;
                        db.ff_user.Add(objRegister);
                        db.SaveChanges();

                        ff_login objLogin = new ff_login();
                        objLogin.email = objUser.email;
                        objLogin.user_id = objRegister.id;
                        objLogin.token = Guid.NewGuid().ToString();
                        objLogin.password = objUser.password;
                        objLogin.created_date = DateTime.Now;

                        db.ff_login.Add(objLogin);
                        db.SaveChanges();

                        result.data = objLogin.token;
                        result.success = true;

                    }
                    else
                    {
                        result.data = email_check.token;
                        result.success = true;

                    }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "SignUp";
                    result.error.error = "Values can not be null or empty.";

                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Create User";
                result.error.error = e.Message.ToString();
            }
            return result;

        }

        //added by nitesh panjwani

        [Route("EditUser")]
        [HttpPost]
        public ApiResult EditUser(Registration objUser)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                if (!string.IsNullOrEmpty(objUser.fname) && !string.IsNullOrEmpty(objUser.lname) && !string.IsNullOrEmpty(objUser.email) && !string.IsNullOrEmpty(objUser.contact) && !string.IsNullOrEmpty(objUser.password))
                {
                    ff_login email_check = db.ff_login.Where(e => e.token.ToLower().Trim() == objUser.auth_token.ToLower().Trim() && e.deleted_date == null).FirstOrDefault();
                    if (email_check != null)
                    {
                        ff_user objRegister = db.ff_user.Where(e => e.id == email_check.user_id && e.deleted_date == null).FirstOrDefault();
                        objRegister.fname = objUser.fname;
                        objRegister.lname = objUser.lname;
                        objRegister.email = objUser.email;
                        objRegister.contact = objUser.contact;
                        objRegister.user_image = objUser.user_image;
                       
                        email_check.email = objUser.email;
                        email_check.password = objUser.password;

                        db.SaveChanges();

                        result.data = email_check.token;
                        result.success = true;

                    }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "EditUser";
                    result.error.error = "Values can not be null or empty.";

                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Edit User";
                result.error.error = e.Message.ToString();
            }
            return result;

        }

        [Route("AddProject")]
        [HttpPost]
        public ApiResult AddProject(ProjectRegistration objProjectRegistration)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                ff_project proj = db.ff_project.Where(e => e.name.ToLower().Trim() == objProjectRegistration.name.ToLower().Trim()).FirstOrDefault();
                if (proj == null)
                {
                    if (!string.IsNullOrEmpty(objProjectRegistration.name))
                    {
                        ff_login user = db.ff_login.Where(e => e.email == objProjectRegistration.user_email).FirstOrDefault();

                        if (user != null)
                        {
                            ff_client client = db.ff_client.Where(e => e.auth_token == objProjectRegistration.client_auth_token).FirstOrDefault();
                            if (client != null)
                            {
                                ff_project objRegister = new ff_project();

                                objRegister.user_id = user.user_id;
                                objRegister.client_id = client.id;

                                objRegister.name = objProjectRegistration.name;
                                objRegister.title = objProjectRegistration.title;
                                objRegister.description = objProjectRegistration.description;
                                objRegister.status = "open";
                                objRegister.auth_token = Guid.NewGuid().ToString();

                                objRegister.created_date = DateTime.Now;
                                db.ff_project.Add(objRegister);
                                db.SaveChanges();

                                result.data = objRegister.auth_token;
                                result.success = true;
                            }
                            else
                            {
                                result.success = false;
                                result.error = new ApiResult.api_error();
                                result.error.heading = "AddProject";
                                result.error.error = "Client doesn't exist. Please enter valid client token.";
                            }
                        }
                        else
                        {
                            result.success = false;
                            result.error = new ApiResult.api_error();
                            result.error.heading = "AddProject";
                            result.error.error = "User doesn't exist. Please enter valid user email address.";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.error = new ApiResult.api_error();
                        result.error.heading = "AddProject";
                        result.error.error = "Values can not be null or empty.";
                    }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "AddProject";
                    result.error.error = "Project with same name already exists. Please enter different name.";
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Create Project";
                result.error.error = e.Message.ToString();
            }
            return result;
        }

        //[Route("EditProject")]
        //[HttpPost]
        //public ApiResult EditProject(ProjectRegistration objProjectRegistration)
        //{
        //    ApiResult result = new ApiResult();
        //    FastFormsDBEntities db = new FastFormsDBEntities();
        //    try
        //    {
        //        ff_project proj = db.ff_project.Where(e => e.auth_token.ToLower().Trim() == objProjectRegistration.auth_token.ToLower().Trim()
        //        && e.deleted_date == null).FirstOrDefault();
        //        if (proj != null)
        //        {
        //            if (!string.IsNullOrEmpty(objProjectRegistration.name))
        //            {
        //                proj.name = objProjectRegistration.name;
        //                proj.title = objProjectRegistration.title;
        //                proj.description = objProjectRegistration.description;
        //                proj.status = "open";
        //                db.SaveChanges();

        //                result.data = proj.auth_token;
        //                result.success = true;
        //            }
        //            else
        //            {
        //                result.success = false;
        //                result.error = new ApiResult.api_error();
        //                result.error.heading = "EditProject";
        //                result.error.error = "Name can not be null or empty.";
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        result.success = false;
        //        result.error = new ApiResult.api_error();
        //        result.error.heading = "Edit Project";
        //        result.error.error = e.Message.ToString();
        //    }
        //    return result;
        //}

        [Route("AddClient")]
        [HttpPost]
        public ApiResult AddClient(ClientRegistration objClientRegistration)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                ff_client objClient = db.ff_client.Where(e => e.email.ToLower().Trim() == objClientRegistration.email.ToLower().Trim()
                 && e.name.ToLower().Trim() == objClientRegistration.name.ToLower().Trim()).FirstOrDefault();
                if (objClient == null)
                {
                    if (!string.IsNullOrEmpty(objClientRegistration.name) && !string.IsNullOrEmpty(objClientRegistration.email))
                    {
                        ff_login user = db.ff_login.Where(e => e.email == objClientRegistration.user_email).FirstOrDefault();

                        if (user != null)
                        {
                            ff_client objRegister = new ff_client();

                            objRegister.user_id = user.user_id;

                            objRegister.name = objClientRegistration.name;
                            objRegister.email = objClientRegistration.email;
                            objRegister.password = objClientRegistration.password;
                            objRegister.contact = objClientRegistration.contact;
                            objRegister.is_active = "yes";
                            objRegister.auth_token = Guid.NewGuid().ToString();

                            objRegister.created_date = DateTime.Now;
                            db.ff_client.Add(objRegister);
                            db.SaveChanges();

                            result.data = objRegister.auth_token;
                            result.success = true;
                        }
                        else
                        {
                            result.success = false;
                            result.error = new ApiResult.api_error();
                            result.error.heading = "AddClient";
                            result.error.error = "User doesn't exist. Please enter valid user email.";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.error = new ApiResult.api_error();
                        result.error.heading = "AddClient";
                        result.error.error = "Values can not be null or empty.";
                    }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "AddClient";
                    result.error.error = "Client with same name already exists. Please enter different name.";
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Create Client";
                result.error.error = e.Message.ToString();
            }
            return result;
        }

        [Route("EditClient")]
        [HttpPost]
        public ApiResult EditClient(ClientRegistration objClientRegistration)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                ff_client objClient = db.ff_client.Where(e => e.auth_token.ToLower().Trim() == objClientRegistration.auth_token.ToLower().Trim()
                && e.deleted_date == null).FirstOrDefault();
                if (objClient != null)
                {
                    if (!string.IsNullOrEmpty(objClientRegistration.name) && !string.IsNullOrEmpty(objClientRegistration.email))
                    {
                        objClient.name = objClientRegistration.name;
                        objClient.email = objClientRegistration.email;
                        objClient.password = objClientRegistration.password;
                        objClient.contact = objClientRegistration.contact;
                        objClient.is_active = "yes";

                        db.SaveChanges();

                        result.data = objClient.auth_token;
                        result.success = true;
                    }
                    else
                    {
                        result.success = false;
                        result.error = new ApiResult.api_error();
                        result.error.heading = "EditClient";
                        result.error.error = "Domain name and Company name can not be null or empty.";
                    }
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Edit Client";
                result.error.error = e.Message.ToString();
            }
            return result;
        }


        [Route("SignUpV2")]
        [HttpPost]
        public ApiResult SignUpV2(Registration objUser)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                if (!string.IsNullOrEmpty(objUser.fname) && !string.IsNullOrEmpty(objUser.lname) && !string.IsNullOrEmpty(objUser.email) && !string.IsNullOrEmpty(objUser.password))
                {
                    var email_check = db.ff_login.Where(e => e.email == objUser.email && e.deleted_date == null).FirstOrDefault();
                    if (email_check == null)
                    {
                        ff_client objClient = db.ff_client.Where(e => e.auth_token.ToLower().Trim() == objUser.client_token.ToLower().Trim()
                         && e.deleted_date == null).FirstOrDefault();

                        ff_project objProject = db.ff_project.Where(e => e.auth_token.ToLower().Trim() == objUser.project_token.ToLower().Trim()
                         && e.deleted_date == null).FirstOrDefault();

                        ff_login parentUser  = db.ff_login.Where(e => e.token.ToLower().Trim() == objUser.auth_token.ToLower().Trim()
                         && e.deleted_date == null).FirstOrDefault();

                        ff_user objRegister = new ff_user();
                        objRegister.fname = objUser.fname;
                        objRegister.lname = objUser.lname;
                        objRegister.email = objUser.email;
                        objRegister.contact = objUser.contact;
                        objRegister.user_image = objUser.user_image;
                        objRegister.parent_user_id = parentUser.user_id;
                        objRegister.created_date = DateTime.Now;

                        db.ff_user.Add(objRegister);

                        db.SaveChanges();

                        ff_login objLogin = new ff_login();
                        objLogin.email = objUser.email;
                        objLogin.user_id = objRegister.id;
                        objLogin.token = Guid.NewGuid().ToString();
                        objLogin.password = objUser.password;
                        objLogin.created_date = DateTime.Now;

                        db.ff_login.Add(objLogin);

                        db.SaveChanges();

                        ff_user_clients objUserClient = new ff_user_clients();
                        objUserClient.user_id = objRegister.id;
                        objUserClient.client_id = objClient.id;

                        db.ff_user_clients.Add(objUserClient);

                        db.SaveChanges();

                        ff_project_users objUserProject = new ff_project_users();
                        objUserProject.user_id = objRegister.id;
                        objUserProject.project_id = objProject.id;

                        db.ff_project_users.Add(objUserProject);

                        db.SaveChanges();

                        result.data = objLogin.token;
                        result.success = true;

                    }
                    else
                    {
                        // for assigning multiple events to a user in EPP
                         
                        ff_login objLogin = db.ff_login.Where(e => e.email == objUser.email && e.deleted_date == null).FirstOrDefault();
                        ff_project objProject = db.ff_project.Where(e => e.auth_token.ToLower().Trim() == objUser.project_token.ToLower().Trim()
                        && e.deleted_date == null).FirstOrDefault();
                        ff_client objClient = db.ff_client.Where(e => e.auth_token.ToLower().Trim() == objUser.client_token.ToLower().Trim()
                        && e.deleted_date == null).FirstOrDefault();

                        ff_user_clients objUserClient = new ff_user_clients();
                        objUserClient.user_id = objLogin.ff_user.id;
                        objUserClient.client_id = objClient.id;

                        db.ff_user_clients.Add(objUserClient);

                        db.SaveChanges();

                        ff_project_users objUserProject = new ff_project_users();
                        objUserProject.user_id = objLogin.ff_user.id;
                        objUserProject.project_id = objProject.id;

                        db.ff_project_users.Add(objUserProject);

                        // for assigning multiple events to a user in EPP

                        db.SaveChanges();

                        result.data = email_check.token;
                        result.success = true;
                    }
                }
                else
                {
                    result.success = false;
                    result.error = new ApiResult.api_error();
                    result.error.heading = "SignUp";
                    result.error.error = "Values can not be null or empty.";

                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Create User";
                result.error.error = e.Message.ToString();
            }
            return result;

        }

        [Route("EditProject")]
        [HttpPost]
        public ApiResult EditProject(ProjectRegistration objProjectRegistration)
        {
            ApiResult result = new ApiResult();
            FastFormsDBEntities db = new FastFormsDBEntities();
            try
            {
                ff_login user = db.ff_login.Where(e => e.email == objProjectRegistration.user_email).FirstOrDefault();

                ff_client client = db.ff_client.Where(e => e.auth_token == objProjectRegistration.client_auth_token
                 && e.deleted_date == null).FirstOrDefault();
                if (client != null)
                {
                    ff_project proj = db.ff_project.Where(x => x.client_id == client.id && x.user_id == user.user_id).FirstOrDefault();

                    if (proj != null)
                    {
                        if (!string.IsNullOrEmpty(objProjectRegistration.name))
                        {
                            proj.name = objProjectRegistration.name;
                            proj.title = objProjectRegistration.title ;
                            proj.description = objProjectRegistration.description ;
                            proj.status = proj.status;
                            db.SaveChanges();

                            result.data = proj.auth_token;
                            result.success = true;
                        }
                        else
                        {
                            result.success = false;
                            result.error = new ApiResult.api_error();
                            result.error.heading = "EditProject";
                            result.error.error = "Name can not be null or empty.";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.error = new ApiResult.api_error();
                        result.error.heading = "EditProject";
                        result.error.error = "Project not found.";
                    }
                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.error = new ApiResult.api_error();
                result.error.heading = "Edit Project";
                result.error.error = e.Message.ToString();
            }
            return result;
        }

        //added by nitesh panjwani
    }
}
