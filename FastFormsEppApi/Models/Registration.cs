﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FastForms.Api.Models
{
    public class Registration
    {
        public decimal id { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string contact { get; set; }
        public string user_image { get; set; }
        public string auth_token { get; set; }
        public string client_token { get; set; }
        public string project_token { get; set; }

        public DateTime created_date { get; set; }
        public DateTime deleted_date { get; set; }
    }

    //added by nitesh panjwani

    public class ProjectRegistration
    {
        public decimal id { get; set; }       
        public string auth_token { get; set; }

        public string name { get; set; }
        public string title { get; set; }
        public string description { get; set; }

        public string user_email { get; set; } // logged in user email so as to get id of user
        public string client_auth_token { get; set; } // to get client id 

        public DateTime created_date { get; set; }
        public DateTime deleted_date { get; set; }
    }

    public class ClientRegistration
    {
        public decimal id { get; set; }
        public string auth_token { get; set; }

        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string contact { get; set; }

        public string is_active { get; set; } 
        public string user_email { get; set; } // logged in user email so as to get id of user

        public DateTime created_date { get; set; }
        public DateTime deleted_date { get; set; }
    }

    public class EditForm
    {
        public decimal id { get; set; }
        public string attr_id { get; set; }
        public string name { get; set; }
        public string message { get; set; }
        public string created_by { get; set; }
        public string is_active { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string form_link { get; set; }
        public string forms_type_id { get; set; }
        public string description { get; set; }

        public string project_token { get; set; }
    }
    //added by nitesh panjwani
}