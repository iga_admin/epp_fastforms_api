﻿using FastForms.Api.Lib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FastForms.Api.Models
{
    public class Form
    {
        public decimal id { get; set; }
        public string attr_id { get; set; }
        public string name { get; set; }
        public string message { get; set; }
        public string created_by { get; set; }
        public string is_active { get; set; }
        public Nullable<DateTime> created_date { get; set; }
        public string form_link { get; set; }
        public string forms_type_id { get; set; }
        public string description { get; set; }


     
    }

}

