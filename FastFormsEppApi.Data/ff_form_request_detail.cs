//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FastFormsEppApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ff_form_request_detail
    {
        public ff_form_request_detail()
        {
            this.ff_form_data = new HashSet<ff_form_data>();
            this.ff_form_data_version = new HashSet<ff_form_data_version>();
        }
    
        public decimal id { get; set; }
        public Nullable<decimal> form_request_id { get; set; }
        public Nullable<System.DateTime> filled_date { get; set; }
    
        public virtual ICollection<ff_form_data> ff_form_data { get; set; }
        public virtual ICollection<ff_form_data_version> ff_form_data_version { get; set; }
        public virtual ff_form_request ff_form_request { get; set; }
    }
}
