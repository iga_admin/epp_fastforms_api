//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FastFormsEppApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ff_user_clients
    {
        public decimal id { get; set; }
        public Nullable<decimal> user_id { get; set; }
        public Nullable<decimal> client_id { get; set; }
        public Nullable<System.DateTime> deleted_date { get; set; }
    
        public virtual ff_client ff_client { get; set; }
        public virtual ff_user ff_user { get; set; }
    }
}
