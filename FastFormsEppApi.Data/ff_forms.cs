//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FastFormsEppApi.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ff_forms
    {
        public ff_forms()
        {
            this.ff_form_fields = new HashSet<ff_form_fields>();
            this.ff_form_request = new HashSet<ff_form_request>();
            this.ff_form_version = new HashSet<ff_form_version>();
        }
    
        public decimal id { get; set; }
        public string form_attr_id { get; set; }
        public string title { get; set; }
        public Nullable<decimal> project_id { get; set; }
        public string description { get; set; }
        public Nullable<decimal> created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<System.DateTime> deleted_date { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string is_active { get; set; }
        public Nullable<decimal> forms_type_id { get; set; }
        public string message { get; set; }
    
        public virtual ICollection<ff_form_fields> ff_form_fields { get; set; }
        public virtual ICollection<ff_form_request> ff_form_request { get; set; }
        public virtual ff_form_type ff_form_type { get; set; }
        public virtual ICollection<ff_form_version> ff_form_version { get; set; }
        public virtual ff_project ff_project { get; set; }
    }
}
